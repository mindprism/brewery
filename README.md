# ArchiStack

## Yodiz Commit tagging

'@' sign represents existence of a notation.

'T' represents Task, 'B' represents Bug. 

'T2" Numeric value after 'T' is the task Id. 

'R' Character after colon tells the system to mark the task 'done' on git push. 

Note:
You can skip ':R' characters from the notation if you do not want to mark the task done.

Example:
@T2:R Blah
